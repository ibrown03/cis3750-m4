package academics;
import java.util.Map;
import java.util.ArrayList;
// import academics.returns.*;

public class TAProfile extends StudentProfile {
  private String userTAPortal;
  private String passTAPortal;

  public TAProfile(String ID, String name, String email, String major,
                   String minor, ArrayList<Schedule> schedHistory, Schedule currSched,
                   Transcript transcript) {
    super(ID, name, email, major, minor, schedHistory, currSched, transcript);
    // initialize TAProfile-specific instance variables
    this.userTAPortal = "";
    this.passTAPortal = "";
  }

  public RtnMsg mergeSchedule() {
    // implementation of merging logic here
    // ...
    return new Msg_Success("temp");
  }

  public boolean isTAPortalLinked() {
    return !userTAPortal.isEmpty() && !passTAPortal.isEmpty();
  }

  private RtnMsg linkTAPortal() {
    // implementation of linking logic here
    // ...
    return new Msg_Success("temp");
  }

  private void newSchedule(String JSON) {
    // Schedule newSched = new Schedule(JSON);
    // this.currSched = newSched;
    // this.schedHistory.add(newSched);
  }
}
