package academics;

class Course {
  private String name;
  private String courseID;
  private String sem;
  private String profName;
  private float weight;
  private String[] timeslots;
  private String[] labSections;

  public Course(String name, String courseID, String sem, String profName,
                float weight) {
    this.name = name;
    this.courseID = courseID;
    this.sem = sem;
    this.profName = profName;
    this.weight = weight;
    this.timeslots = new String[7]; // this might need to be updated to be passed in the constructor
    this.labSections = new String[7];
  }

  public String name(){
    return this.name;
  }

  public float weight(){
    return this.weight;
  }




}
