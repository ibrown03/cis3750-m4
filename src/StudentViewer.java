package academics;
// import academics.returns.*;

public class StudentViewer {
  private StudentProfile[] studentProfileArray;

  public StudentViewer(StudentProfile[] stuList) {
    this.studentProfileArray = stuList;
  }

  // studentProfileArray needs to be accessed using a private method to "getStudentProfileArray" as per good OO
  private Boolean studentExists(String ID) {
    for (StudentProfile profile : studentProfileArray) {
      if (profile.getStudentID().equals(ID)) {
        return true;
      }
    }
    return false;
  }

  public StudentProfile findStudent(String ID) {
    for (StudentProfile profile : studentProfileArray) {
      if (profile.getStudentID().equals(ID)) {
        return profile;
      }
    }
    return null;
  }

  public RtnMsg viewGrades(String ID, String startRange, String endRange) {
    if (studentExists(ID)) {
      StudentProfile student = findStudent(ID);
      return student.viewGrades(startRange, endRange);
    } else {
      return new RtnMsg(false, "Student not found");
    }
  }

  public RtnMsg viewCourses(String ID) {
    if (studentExists(ID)) {
      StudentProfile student = findStudent(ID);
      return student.viewCourses();
    } else {
      return new RtnMsg(false, "Student not found");
    }
  }
}
