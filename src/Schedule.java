package academics;
import java.util.ArrayList;
import java.util.Map;
import java.io.FileWriter;
import java.io.IOException;

public class Schedule {
  Map<String, ArrayList<String>> weekSchedule;
  private String sem;
  private String[] days = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

  public Schedule(Map<String, ArrayList<String>> weekSchedule, String sem) {
    this.weekSchedule = weekSchedule;
    this.sem = sem;
  }

  public RtnMsg merge(Schedule temp) {
    Map<String, ArrayList<String>> tempSchedule = temp.getWeekSchedule();

    for (Map.Entry<String, ArrayList<String>> entry:tempSchedule.entrySet()) {

      ArrayList<String> daySched= entry.getValue();
      addToDay(i, daySched);
    }

    /*
    for (int i = 0; i < 7; i++) {
      
      ArrayList<String> daySched = tempSchedule.get(i);
      addToDay(i, daySched);
    }
    */
      
    temp = null;
    return new RtnMsg(true, "Schedule merged successfully");
  }

  private void addToDay(int day, ArrayList<String> daySched) {
    // ArrayList<String> currSched = weekSchedule.get(days[day]);
    for (String entry : daySched) {
      // currSched.add(entry);
      (weekSchedule.get(days[day])).add(entry);
    }
    // weekSchedule.get(days.get(day)) = currSched;
  }

  public RtnMsg export(String fType, String fName) {
    try {
      if (fType.equals("csv")) {
        String filename = fName + "." + fType;
        FileWriter myWriter = new FileWriter(filename);
        
        for (Map.Entry<String, ArrayList<String>> entry:weekSchedule.entrySet()) {
          myWriter.write(entry.getKey() + ", ");
  
          for (String text : entry.getValue()) {
            text = text.replace(',', ' ');
            myWriter.write(text + ", ");
          }

          myWriter.write("\n");
        }

        myWriter.close();
      }

      else if (fType.equals("txt")) {
        String filename = fName + "." + fType;
        FileWriter myWriter = new FileWriter(filename);
        
        for (Map.Entry<String, ArrayList<String>> entry:weekSchedule.entrySet()) {
          myWriter.write(entry.getKey() + ":\n");
  
          for (String text : entry.getValue()) {
            myWriter.write("\t" + text + "\n");
          }

          myWriter.write("\n");
        }

        myWriter.close();
      }

      else {
        return new RtnMsg(false, "Invalid file type chosen. Failed to export schedule.");
      }
      
    } catch (IOException e) {
      return new RtnMsg(false, "An error occurred.");
      e.printStackTrace();
    }
    // implementation details depend on the specific file type
    return new RtnMsg(true, "Schedule exported successfully");
  }

  private ArrayList<String> daySchedule(int day) {
    return weekSchedule.get(days[day]);
  }

  //getters
  public Map<String, ArrayList<String>> getWeekSchedule() {
    return weekSchedule; 
  }
    
  public String getSem() {
    return sem;
  }

  public String[] getDays() {
    return days;
  }


  //setters
  public void setWeekSchedule (Map<String, ArrayList<String>> newWeekSchedule) {
    weekSchedule = newWeekSchedule;
  }

  public void setSem (String newSem) {
    sem = newSem;
  }
}
