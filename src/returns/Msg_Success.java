package academics;
public class Msg_Success implements RtnMsg {
    private String successMsg;

    public Msg_Success(String Msg) {
        this.successMsg = Msg;
    }

    @Override
    public boolean isSuccess() {
        return true;
    }
    
    @Override
    public String getReturnMsg() {
        return successMsg; // or return an empty string ""
    }
}
