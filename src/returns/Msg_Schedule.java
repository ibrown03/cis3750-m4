package academics;
import academics.Schedule;
public class Msg_Schedule extends Msg_Success {
    private Schedule sched;

    public Msg_Schedule(Schedule sched) {
        super("Gotten schedules");
        this.sched = sched;
    }

    public Schedule getSchedule() {
        return sched;
    }
}
