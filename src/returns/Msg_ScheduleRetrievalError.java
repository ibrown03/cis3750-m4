package academics;
// Declare the Msg_ScheduleRetrievalError class, which extends Msg_Failure
class Msg_ScheduleRetrievalError extends Msg_Failure {
  // Define the constructor for Msg_ScheduleRetrievalError
  public Msg_ScheduleRetrievalError() {
    super("Error: Schedule retrieval failed");
  }
}
