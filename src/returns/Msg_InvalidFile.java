package academics;
// Declare the Msg_InvalidFile class, which extends Msg_Failure
class Msg_InvalidFile extends Msg_Failure {
  // Define the constructor for Msg_InvalidFile
  public Msg_InvalidFile() {
    super("Error: Invalid file format");
  }
}
