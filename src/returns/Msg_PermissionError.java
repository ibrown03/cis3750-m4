package academics;
// Declare the Msg_PermissionError class, which extends Msg_Failure
class Msg_PermissionError extends Msg_Failure {
  // Define the constructor for Msg_PermissionError
  public Msg_PermissionError() {
    super("Error: Permission denied");
  }
}
