package academics;
// Declare the Msg_NoSchedule class, which extends Msg_Failure
class Msg_NoSchedule extends Msg_Failure {
  // Define the constructor for Msg_NoSchedule
  public Msg_NoSchedule() {
    super("Error: No schedule found");
  }
}
