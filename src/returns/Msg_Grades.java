package academics;
import java.util.Map;
import academics.Course;
// Declare the Msg_Grades class, which extends Msg_Success
class Msg_Grades extends Msg_Success {
  // Define the instance variable for Msg_Grades
  private Map<Course, Integer> grades;

  // Define the constructor for Msg_Grades
  public Msg_Grades(Map<Course, Integer> grades) {
    super("Gotten grades");
    this.grades = grades;
  }

  // Define the getGrades method for Msg_Grades
  public Map<Course, Integer> getGrades() {
    return this.grades;
  }
}
