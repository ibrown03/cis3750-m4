package academics;
// Declare the Msg_InvalidSem class, which extends Msg_Failure
  class Msg_InvalidSem extends Msg_Failure {
    // Define the constructor for Msg_InvalidSem
    public Msg_InvalidSem() {
      super("Error: Invalid semester");
    }
  }
