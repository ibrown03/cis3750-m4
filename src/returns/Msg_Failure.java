package academics;
public class Msg_Failure implements RtnMsg {
    private String errorMsg;

    // Default constructor
    public Msg_Failure() {
        super();
        this.errorMsg = "Unknown error";
    }

    // Constructor with error message parameter
    public Msg_Failure(String errorMsg) {
        super();
        this.errorMsg = errorMsg;
    }

    @Override
    public boolean isSuccess() {
        return false;
    }

    @Override
    public String getReturnMsg() {
        return errorMsg;
    }
}
