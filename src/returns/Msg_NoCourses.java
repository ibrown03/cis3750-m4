package academics;
// Declare the Msg_NoCourses class, which extends Msg_Failure
class Msg_NoCourses extends Msg_Failure {
  // Define the constructor for Msg_NoCourses
  public Msg_NoCourses() {
    super("Error: No courses found for the specified student");
  }
}
