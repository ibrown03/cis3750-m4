package academics;
import academics.Course;
public class Msg_Courses extends Msg_Success {
    private Course[] courses;
    
    public Msg_Courses(Course[] courses) {
        super("Gotten courses");
        this.courses = courses;
    }
    
    public Course[] getCourses() {
        return courses;
    }
}
