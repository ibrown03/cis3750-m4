package academics;
// Declare the Msg_InvalidStudent class, which extends Msg_Failure
public class Msg_InvalidStudent extends Msg_Failure {
  // Define the constructor for Msg_InvalidStudent
  public Msg_InvalidStudent() {
    super("Error: Invalid student ID");
  }
}
