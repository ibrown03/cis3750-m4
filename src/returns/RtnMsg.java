package academics;
public interface RtnMsg {
    String getReturnMsg();

    boolean isSuccess();
}
