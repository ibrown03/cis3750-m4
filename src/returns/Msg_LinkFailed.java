package academics;
// Declare the Msg_LinkFailed class, which extends Msg_Failure
class Msg_LinkFailed extends Msg_Failure {
    // Define the constructor for Msg_LinkFailed
    public Msg_LinkFailed() {
        super("Error: Could not create link");
    }
}
