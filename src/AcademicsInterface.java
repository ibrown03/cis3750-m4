package academics;
import external.ExternalInterface;
import external.EmailService;
import other.LoginManagerInterface;
import java.util.Map;
// import returns.*;

public class AcademicsInterface {
  StudentViewer students;
  ExternalInterface extern;
  LoginManagerInterface login;

// THIS CONSTRUCTOR IS BAD AND NOT OO HAPPY, FIX ONCE EVERYTHING ELSE IS WORKING
  public AcademicsInterface(StudentViewer students){
    this.students = students;
    this.extern = new ExternalInterface();
    this.login = new LoginManagerInterface("1111111"); //might need a better way of passing this in
  }


  public String viewPreviousCourseGrades(String ID, String startRange, String endRange) {
    RtnMsg rtn = students.viewGrades(ID, startRange, endRange);
    if(rtn.isSuccess()){
      // parse using parseGrades()
    }
    else{
      return rtn.getReturnMsg();
    }

    System.out.println("\nGrades from F21 - F22:\n");
    System.out.println("F21:");
    System.out.println("\tAlgebra: 80");
    System.out.println("\tBiochemistry: 67");
    System.out.println("\tProgramming: 22");
    System.out.println("W22:");
    System.out.println("\tLinear Algebra: 92");
    System.out.println("\tSociology: 65");
    System.out.println("\tMicrocomputers: 77");
    System.out.println("F22:");
    System.out.println("\tSoftware Design: 33");
    System.out.println("\tPsychology: 55");
    System.out.println("\tStatistics: 85");
  }

  // TODO: UPDATE VIEW MODEL?
  public String changeMinor(String minor) {
    String ID = login.getLoggedInUser();
    StudentProfile stu = students.findStudent(ID);
    EmailService email = extern.emailService();
    stu.requestChangeMinor(minor, email);
    
    return "An email has been sent to your student counsellor, please monitor your email for a notification on the status of your minor change.";
  }

  public String changeMajor(String major) {
    String ID = login.getLoggedInUser();
    StudentProfile stu = students.findStudent(ID);
    EmailService email = extern.emailService();
    stu.requestChangeMinor(major, email);
    
    return "An email has been sent to your student counsellor, please monitor your email for a notification on the status of your major change.";
  }

  // TODO: update to include check if correct profile type?
  public void mergeTASchedule() { 
    String ID = login.getLoggedInUser();
    StudentProfile stu = students.findStudent(ID);
    RtnMsg rtn;
    if(stu instanceof TAProfile){
      TAProfile ta = (TAProfile)stu;
      rtn = ta.mergeSchedule();   // this might need heavy changing depending how we handling linkTAPortal
    }
    
    // finish, get return message based on the RtnMsg info? 

    // System.out.println("\nSchedules merged."); 
  }

  // this needs finishing based on the TODO comment
  public String exportClassSchedule(String sem, String fType, String fName) {
    String ID = login.getLoggedInUser();
    StudentProfile stu = students.findStudent(ID);
    RtnMsg rtn = stu.exportSchedule(sem, fType, fName);

    // TODO: update this to just return rtn.getReturnMsg() if the RtnMsg is updated as such
    return (rtn.isSuccess() ? "Class schedule exported successfully" : rtn.getReturnMsg());

    // System.out.println("\nclass-schedule-f23.csv exported to ~/downloads.");
  }

  // THIS NEEDS TO BE REMOVED, 
  public void linkTAPortalWithGryphWeb() {
    System.out.println("\nTA portal successfully linked.");
  }

  // this needs to be finished -- relies on return types so waiting for that
  public void viewCompletedCourses(String ID) {
    RtnMsg rtn = students.viewCourses(ID);
    // add logic based on finished return types
    // call parseCourseList


    System.out.println("\nCourses completed:\n");
    System.out.println("\tAlgebra (0.5)");
    System.out.println("\tBiochemistry (0.5)");
    System.out.println("\tLinear Algebra (0.5)");
    System.out.println("\tMicrocomputers (1.0)");
    System.out.println("\tProgramming (0.5)");
    System.out.println("\tPsychology (0.5)");
    System.out.println("\tSociology (0.5)");
    System.out.println("\tSoftware Design (0.75)");
    System.out.println("\tStatistics (0.5)");
  }

  // this needs to be finished -- relies on return types so waiting for that
  public void viewClassSchedule(String sem) {
    String ID = login.getLoggedInUser();
    StudentProfile stu = students.findStudent(ID);
    RtnMsg rtn = stu.viewSchedule(sem);
    // add logic based on finished return types
    // call parseSchedule


    System.out.println("\nClass schedules for W23:\n");
    System.out.println("Monday:");
    System.out.println("\tLinear Algebra (MATH*1160 01)");
    System.out.println("\t(Doe, J.) (ROZ 100) - 14:30-15:50\n");
    System.out.println("\tPhilosophy Of Time (PHIL*1300 08)");
    System.out.println("\t(Finneas, P.) (MCN 2100) - 16:00-17:20\n");
    System.out.println("Tuesday:");
    System.out.println("\tNone.\n");
    System.out.println("Wednesday:");
    System.out.println("\tSoftware Design IV (CIS*4250 DE)");
    System.out.println("\t(Carleson, F.) (Distance Education) - 13:00-14:20\n");
    System.out.println("Thursday:");
    System.out.println("\tEthics (PHIL*2300 03)");
    System.out.println("\t(Pete, S.) (MCK 1200) - 13:00-14:20\n");
    System.out.println("\tSystems Design (CIS*3750 05)");
    System.out.println("\t(Miles, G.) (REY 1000) - 17:00-18:20\n");
    System.out.println("\tGerman (GERM*1000 DE)");
    System.out.println("\t(Purnell, R.) (Distance Education) - 19:00-20:20\n");
    System.out.println("Friday:");
    System.out.println("\tDiscrete Structures (CIS*1910 07)");
    System.out.println("\t(Svaden, J.) (THRN 3010) - 08:30-09:20\n");
    System.out.println("\tMicroeconomics (ECON*1100 DE)");
    System.out.println("\t(Adrone, D.) (Distance Education) - 19:00-19:50");
  }


  private String parseCourseList(Course[] courses) {
    String parsed = "Courses Completed:\n";
    for(Course course : courses){
      parsed += course.name() + " (" + course.weight() +")\n";
    }

    return parsed;
  }

  // this needs to be finished -- dependent on Schedule class being finished
  private String parseSchedule(Schedule sched){
    String parsed = "Class Schedule for " + sched.getSem() + ":\n";
    // NEEDS TO WAIT FOR SCHEDULE TO BE IMPLEMENTED -- DON'T KNOW HOW TO LOOP YET
  }

  // this does not specify which semester each grade is from yet, is that needed? Or update UI doc
  private String parseGrades(Map<Course, Integer> grades, String startRange, String endRange){
    String parsed = "Grades from " + startRange + " - " + endRange + ":\n";
    for(Map.Entry<Course, Integer> grade : grades.entrySet()){
      parsed += grade.getKey().name() + ": " + grade.getValue() + "\n";
    }

    return parsed;
  }
}
