import java.util.Scanner;
import academics.*;

public class CommandLineInterface {
  public static void main(String[] args) {

    StudentProfile[] stuList = {};
    StudentViewer students = new StudentViewer(stuList);
    AcademicsInterface academicsInterface = new AcademicsInterface(students);
    Scanner scanner = new Scanner(System.in);

    while (true) {
      System.out.println("\n------------------");
      System.out.println("- Academics Menu -");
      System.out.println("------------------\n");
      System.out.println("1. View Previous Course Grades");
      System.out.println("2. Change Minor");
      System.out.println("3. Change Major");
      System.out.println("4. Merge TA Schedule");
      System.out.println("5. Export Class Schedule");
      System.out.println("6. View Completed Courses");
      System.out.println("7. View Class Schedule");
      System.out.println("8. Exit program\n");

      System.out.print("Enter your selection: ");
      try {
        int choice = scanner.nextInt();

        switch (choice) {
        case 1:
          System.out.print("Enter student ID: ");
          String studentID = scanner.next();
          System.out.print("Enter term: ");
          String term = scanner.next();
          System.out.print("Enter year: ");
          String year = scanner.next();
          academicsInterface.viewPreviousCourseGrades(studentID, term, year);
          break;
        case 2:
          System.out.print("Enter new minor: ");
          String minor = scanner.next();
          academicsInterface.changeMinor(minor);
          break;
        case 3:
          System.out.print("Enter new major: ");
          String major = scanner.next();
          academicsInterface.changeMajor(major);
          break;
        case 4:
          academicsInterface.mergeTASchedule();
          break;
        case 5:
          System.out.print("Enter term: ");
          String exportTerm = scanner.next();
          System.out.print("Enter file format (txt or csv): ");
          String fileFormat = scanner.next();
          System.out.print("Enter file name: ");
          String fileName = scanner.next();
          academicsInterface.exportClassSchedule(exportTerm, fileFormat, fileName);
          break;
        case 6:
          System.out.print("Enter student ID: ");
          String studentID2 = scanner.next();
          academicsInterface.viewCompletedCourses(studentID2);
          break;
        case 7:
          System.out.print("Enter term: ");
          String viewTerm = scanner.next();
          academicsInterface.viewClassSchedule(viewTerm);
          break;
        case 8:
          System.out.println("\nExiting.");
          System.exit(0);
          break;
        default:
          System.out.println("\nInvalid choice. Returning to Academics Menu.");
        }
      } catch (Exception e) {
        System.out.println("\nInvalid input. Returning to Academics Menu.");
        scanner.next(); // consume bad input to avoid infinite loop
      }
    }
  }
}
