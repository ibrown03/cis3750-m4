package other;

// dummy interface for LoginManager subsystem
public class LoginManagerInterface{
    String loggedInID;

    public LoginManagerInterface(String loggedInID){
        this.loggedInID = loggedInID;
    }

    public String getLoggedInUser(){
        return this.loggedInID;
    }
}