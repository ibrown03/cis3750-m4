package academics;
import java.util.ArrayList;

class ScheduleList {
  private ArrayList<Schedule> schedules;

  public ScheduleList(ArrayList<Schedule> schedules) {
    this.schedules = schedules;
  }

  //only returns Msg_NoSchedule vs SD states could return NoCourses --> inconsistent from SDs
  private Schedule schedule(String sem) {
    for (Schedule schedule : schedules) {
      if (schedule.getSem().equals(sem)) {
        return schedule;
      }
    }
    return null; // If no matching schedule is found
  }

  public RtnMsg getSchedule(String sem) {
    Schedule schedule = schedule(sem);
    if(schedule == null) {
      return new Msg_NoSchedule(); // If no matching schedule is found
    }
    return new Msg_Schedule(schedule);
  }

  // updated to be RtnMsg
  public RtnMsg exportSchedule(String sem, String fType, String fName) {
    Schedule schedule = schedule(sem);
    if (schedule == null) {
      return new Msg_NoSchedule();
    }
    return schedule.export(fType, fName);
  }
}
