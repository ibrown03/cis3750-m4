package academics;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Iterator;
import java.util.HashMap;
// import academics.returns.*;
// NOT TESTED YET : (

public class Transcript {
  private Map<Course, Integer> grades;

  public Transcript(Map<Course, Integer> grades) { this.grades = grades; }



  public boolean hasCompletedCourses() {
    for (Map.Entry<Course, Integer> entry : grades.entrySet()) {
      if (entry.getValue() != null && entry.getValue() >= 50) {
        return true;
      }
    }
    return false;
  }


  // updated to be public
  public RtnMsg getCompleteCourses() {
    if (hasCompletedCourses() == false) {
      // if no completed courses just return a RtnMsg 
      return new Msg_NoCourses();
    }
    else {
      // loop through all courses a return a list of the completed courses
      List<Course> completedCourses = new ArrayList<Course>();
      for (Map.Entry<Course, Integer> entry : grades.entrySet()) {
        if (entry.getValue() != null && entry.getValue() >= 50) {
          completedCourses.add(entry.getKey());
        }
      }
      // convert the array list to a normal array of courses
      return new Msg_Courses(completedCourses.toArray(new Course[completedCourses.size()]));
    }
  }



  public RtnMsg collectGrades(String startRange, String endRange) {
    // function to collect all the grades from courses within a given range
    // first get the list of courses that are in the given range 
    Msg_Courses filteredResult = new Msg_Courses();
    //SUPER RUSTY WITH JAVA :( this just points the new object to the returned object from filterCourses()?
    filteredResult = filterCourses(startRange, endRange);
    if (filteredResult.size() == 0) {
      return new Msg_NoCourses();
    }
    // arraylist of course return in RtnMsg
    // convert the return list of courses to an arraylist
    List<Course> coursesInRange = Arrays.asList(filteredResult.getCourses());

    //the map that is to be returned - Msg_Grades will have a map of the grades 
    Map<Course, Integer> filteredGrades = new HashMap<>();

    // loop thorugh alll the courses returned from filterCourses() then add them to a resultant FilterGrades map 
    // key is the Course 
    // value is the grade 
    for(int i = 0; i < coursesInRange.size(); i++) {
        filteredGrades.put(coursesInRange.get(i), grades.get(coursesInRange.get(i)));
    }

    return new Msg_Grades(filteredGrades);
  }


  // startRange and endRange will be in the form:
  // W22 or S23 or F21 etc.. 
  // to filter will split string into seperate vars so easier to compare
  private RtnMsg filterCourses(String startRange, String endRange) {
    // arraylist of course return in RtnMsg
    ArrayList<Course> coursesInRange = new ArrayList<Course>();
    String curCourseSem;
    // looping through the courses
    for (Map.Entry<Course, Integer> entry : grades.entrySet()) {
      //get current course semester Season and Year
      curCourseSem = entry.getValue().getSem();
      // now compare it with the given start and end ranges
      // if curCourse is > start and < end
      if (CompareSemesters(curCourseSem, startRange) == 1 && CompareSemesters(curCourseSem, endRange) == -1) {
        coursesInRange.add(entry.getKey());
      }
      // if curCourse is = start and < end
      else if (CompareSemesters(curCourseSem, startRange) == 0 && CompareSemesters(curCourseSem, endRange) == -1) {
        coursesInRange.add(entry.getKey());
      }
      // if curCourse is > start and = end
      else if (CompareSemesters(curCourseSem, startRange) == 1 && CompareSemesters(curCourseSem, endRange) == 0) {
        coursesInRange.add(entry.getKey());
      } 
      // if curCourse is = start and = end
      else if (CompareSemesters(curCourseSem, startRange) == 0 && CompareSemesters(curCourseSem, endRange) == 0) {
        coursesInRange.add(entry.getKey());
      }
    }
    // if by the end the return map "coursesInRange" is empty then that means no courses were found in given range
    if (CoursesInRange.size() == 0) {
      return new Msg_NoCourses();
    }
    else {
      //return Msg_Courses and pass in the list of courses to contructor - note converting arrraylist to normal array? 
      new Msg_Courses(coursesInRange.toArray(new Course[coursesInRange.size()]));
    }
  }

  // startRange and endRange will be in the form:
  // W22 or S23 or F21 etc.. 
  // to filter will split string into seperate vars so easier to compare
  // -----> return -1 if sem1 < sem2
  // -----> return 0 if sem1 == sem2
  // -----> return 1 if sem1 > sem2
  private Integer CompareSemesters(String sem1, String sem2) {
    // for comparing when filtering
    HashMap<Character, Integer> semSeasons = new HashMap<Character, Integer>();
    semSeasons.put('F', 1);
    semSeasons.put('W', 2);
    semSeasons.put('S', 3);
    try { 
      char sem1Season = sem1.CharAt(0);
      char sem2Season = sem2.CharAt(0);
      int sem1Year = Integer.parseInt(sem1.subString(1, 2));
      int sem2Year = Integer.parseInt(sem2.subString(1, 2));
    }
    catch (NumberFormatException ex) {
      ex.printStackTrace();
    }
    //now compare the two semesters
    if (sem1Year > sem2Year) {
      return 1;
    }
    else if (sem1Year < sem2Year) {
      return -1;
    }
    else {
      if(semSeasons.get(sem1Season) > semSeasons.get(sem2Season)){
        return 1;
      }
      else if (semSeasons.get(sem1Season) < semSeasons.get(sem2Season)){
        return -1;
      }
      else {
        //they equal
        return 0;
      }
    }
  }


}
