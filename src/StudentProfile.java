package academics;
import java.util.ArrayList;
import external.EmailService;
// import academics.returns.*;

public class StudentProfile {
  private String ID;
  private String name;
  private String email;
  private Schedule currSched;
  private ScheduleList schedHistory;
  private Transcript transcript;
  private String major;
  private String minor;

  public StudentProfile(String ID, String name, String email, String major,
                        String minor, ArrayList<Schedule> schedHistory,
                        Schedule currSched, Transcript transcript) {
    this.ID = ID;
    this.name = name;
    this.email = email;
    this.major = major;
    this.minor = minor;
    this.schedHistory = new ScheduleList(schedHistory);
    this.currSched = currSched;
    this.transcript = transcript;
  }

// private methods
  private Transcript transcript(){
    return this.transcript;
  }

  private ScheduleList schedHistory(){
    return this.schedHistory;
  }

// public methods
  public String id(){ // TODO: UPDATE DESIGN -- new public method
    return this.ID;
  }

  public RtnMsg getGrades(String startRange, String endRange) {
    Transcript tran = this.transcript();
    return tran.collectGrades(startRange, endRange);
  }

  public RtnMsg viewSchedule(String sem) {
    return schedHistory.getSchedule(sem);
  }

  public RtnMsg getCompleteCourses() {
    Transcript tran = this.transcript();
    if(!tran.hasCompletedCourses()) {
      return new Msg_NoCourses();
    }

    return tran.getCompleteCourses();
  }

  //TODO: ensure all uses of EmailService work according to prof response email

  public void requestChangeMinor(String minor, EmailService email) {
    String stuID = this.id();
    email.sendNotification(stuID + "Change minor request", 
      stuID + "is making a request to change their minor to: " + minor + 
      "please address this request through GryphWeb.");
  }

  public void requestChangeMajor(String major, EmailService email) {
    String stuID = this.id();
    email.sendNotification(stuID + "Change major request", 
      stuID + "is making a request to change their major to: " + major + 
      "please address this request through GryphWeb.");
  }

  public void changeMinor(String minor, EmailService email) {
    this.minor = minor;
    String stuID = this.id();
    email.sendNotification("Change minor request accepted", 
      stuID + " - Your minor has successfuly been changed to: " + minor);
  }

  public void changeMajor(String major, EmailService email) {
    this.major = major;
    String stuID = this.id();
    email.sendNotification("Change major request accepted", 
      stuID + " - Your major has successfuly been changed to: " + major);
  }

  public void rejectChangeMinor(EmailService email) {
    String stuID = this.id();
    email.sendNotification("Change minor request denied", 
      stuID + " - Your request to change your minor to " + minor +
      "has been denied. Please contact your student councellor for more information.");
  }

  public void rejectChangeMajor(EmailService email) {
    String stuID = this.id();
    email.sendNotification("Change major request denied", 
      stuID + " - Your request to change your major to " + major +
      "has been denied. Please contact your student councellor for more information.");
  }

  public RtnMsg exportSchedule(String sem, String fType, String fName) {
    ScheduleList schedules = this.schedHistory();
    return schedules.exportSchedule(sem, fType, fName);
  }
}
