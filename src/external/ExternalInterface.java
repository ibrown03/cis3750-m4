
// TODO: update design to include this subsystem where needed
package external;
public class ExternalInterface {
    EmailService email;

    public ExternalInterface() {
        email = new EmailService();
    }

    public EmailService emailService(){
        return this.email;
    }
}