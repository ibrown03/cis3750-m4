# Instructions To Compile & Execute Code

- To compile, enter `gradle build`.
- To execute, enter `gradle run --console plain`.